#include <iostream>
#include "Tree.cpp"

using namespace std;


int main(){


    Tree<int>* drzewko = new Tree<int>();


    //WYSOKOSC 0 (ROOT)
    drzewko->addNode(NULL, 32);

    //WYSOKOSC 1
    TreeNode<int>* A = drzewko->addNode(drzewko->getRoot(), 55);

    TreeNode<int>* B = drzewko->addNode(drzewko->getRoot(), 66);

    TreeNode<int>* C = drzewko->addNode(drzewko->getRoot(), 99);

    //WYSOKOSC 2
    TreeNode<int>* D = drzewko->addNode(A, 77);



    /*
    drzewko->removeNode(A);
    drzewko->removeNode(B);

    */

    cout<<"Wysokosc drzewa: "<<drzewko->getHeight(drzewko->getRoot())<<"\n";

    cout<<"PRE ORDER \n";
    drzewko->preOrder(drzewko->getRoot());

    cout<<"POST ORDER \n";
    drzewko->postOrder(drzewko->getRoot());

    return 0;
}

