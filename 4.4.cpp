#include "PriorityQueue.cpp"
#include <iostream>
#include <cstdlib>

using namespace std;

int* tablica;
int n = 15;

PriorityQueue<int>* sort(){
    PriorityQueue<int>* kolejka = new PriorityQueue<int>();

    for(int i = 0; i<n; ++i){
        kolejka->insert(tablica[i], tablica[i]);
    }

    return kolejka;
}

int main(){

    tablica = new int[n];

    for(int i=0; i<n; ++i){
        tablica[i] = rand();
    }

    PriorityQueue<int>* kolejka = sort();

    while(!kolejka->isEmpty()){
        cout<<kolejka->removeMin()<<"\n";
    }

    return 0;
}


