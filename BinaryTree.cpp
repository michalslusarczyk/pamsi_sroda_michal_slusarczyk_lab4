#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

template <typename T>
class BinaryTreeNode{
public:
    T getElement();
    void setElement(T t);

    BinaryTreeNode<T>* getParent();
    BinaryTreeNode<T>* getLeft();
    BinaryTreeNode<T>* getRight();

    void setParent(BinaryTreeNode<T>* p);
    void setLeft(BinaryTreeNode<T>* l);
    void setRight(BinaryTreeNode<T>* r);

    bool isRoot();
    bool isExternal();
private:
    T element;
    BinaryTreeNode<T>* parent;
    BinaryTreeNode<T>* left;
    BinaryTreeNode<T>* right;
};

template <typename T>
T BinaryTreeNode<T>::getElement(){
    return element;
}

template <typename T>
void BinaryTreeNode<T>::setElement(T t){
    element = t;
}

template <typename T>
BinaryTreeNode<T>* BinaryTreeNode<T>::getParent(){
    return parent;
}

template <typename T>
BinaryTreeNode<T>* BinaryTreeNode<T>::getLeft(){
    return left;
}

template <typename T>
BinaryTreeNode<T>* BinaryTreeNode<T>::getRight(){
    return right;
}

template <typename T>
void BinaryTreeNode<T>::setParent(BinaryTreeNode<T>* p){
    parent = p;
}

template <typename T>
void BinaryTreeNode<T>::setLeft(BinaryTreeNode<T>* l){
    left = l;
}

template <typename T>
void BinaryTreeNode<T>::setRight(BinaryTreeNode<T>* r){
    right = r;
}

template <typename T>
bool BinaryTreeNode<T>::isRoot(){
    return parent == NULL;
}

template <typename T>
bool BinaryTreeNode<T>::isExternal(){
    return (left == NULL && right == NULL);
}


//======================================================================

template <typename T>
class BinaryTree{
public:
    BinaryTreeNode<T>* getRoot();
    T getRootElement();

    int getHeight(BinaryTreeNode<T>* node);

    BinaryTreeNode<T>* addNode(BinaryTreeNode<T>* parent, T t);
    T removeNode(BinaryTreeNode<T>* node);

    bool isEmpty();

    void preOrder(BinaryTreeNode<T>* node);
    void inOrder(BinaryTreeNode<T>* node);
    void postOrder(BinaryTreeNode<T>* node);
private:
    BinaryTreeNode<T>* root;
};

template <typename T>
BinaryTreeNode<T>* BinaryTree<T>::getRoot(){
    return root;
}

template <typename T>
T BinaryTree<T>::getRootElement(){
    if(!isEmpty()){
        return root->getElement();
    }else{
        throw "TreeEmptyException";
    }
}

template <typename T>
int BinaryTree<T>::getHeight(BinaryTreeNode<T>* node){
    if(node->isExternal()){
        return 0;
    }else{
        int h = 0;
        if(node->getLeft() != NULL)
            h = std::max(h, getHeight(node->getLeft()));
        if(node->getRight() != NULL)
            h = std::max(h, getHeight(node->getRight()));
        return h+1;
    }
}

template <typename T>
BinaryTreeNode<T>* BinaryTree<T>::addNode(BinaryTreeNode<T>* parent, T t){
    BinaryTreeNode<T>* child = new BinaryTreeNode<T>();
    child->setLeft(NULL);
    child->setRight(NULL);
    child->setElement(t);
    if(isEmpty()){
        child->setParent(NULL);
        root = child;
    }else{
        child->setParent(parent);
        if(parent->getLeft() == NULL){
            parent->setLeft(child);
        }else if(parent->getRight() == NULL){
            parent->setRight(child);
        }else{
            throw "NodeFullException";
        }
    }

    return child;
}

template <typename T>
T BinaryTree<T>::removeNode(BinaryTreeNode<T>* node){
    T temp = node->getElement();
    if(node->getParent() != NULL){
        if(node->getParent()->getLeft() == node){
            node->getParent()->setLeft(NULL);
        }else{
            node->getParent()->setRight(NULL);
        }
    }
    delete node;
    return temp;
}

template <typename T>
bool BinaryTree<T>::isEmpty(){
    return root == NULL;
}

template <typename T>
void BinaryTree<T>::preOrder(BinaryTreeNode<T>* node){
    if(node != NULL){
        cout<<node->getElement()<<"\n";
        preOrder(node->getLeft());
        preOrder(node->getRight());
    }
}

template <typename T>
void BinaryTree<T>::inOrder(BinaryTreeNode<T>* node){
    if(node != NULL){
        inOrder(node->getLeft());
        cout<<node->getElement()<<"\n";
        inOrder(node->getRight());
    }
}

template <typename T>
void BinaryTree<T>::postOrder(BinaryTreeNode<T>* node){
    if(node != NULL){
        postOrder(node->getLeft());
        postOrder(node->getRight());
        cout<<node->getElement()<<"\n";
    }
}
