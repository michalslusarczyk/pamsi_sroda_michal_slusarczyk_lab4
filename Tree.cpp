#include "List.cpp"
#include <iostream>
using namespace std;

template <typename T>
class TreeNode{
public:
    TreeNode();

    T getElement();
    void setElement(T t);

    TreeNode<T>* getParent();
    TreeNode<T>* getChild(int n);

    void setParent(TreeNode<T>* p);
    void addChild(TreeNode<T>* c);
    void removeChild(TreeNode<T>* c);

    int getSize();

    bool isRoot();
    bool isExternal();
private:
    List<TreeNode<T>*>* children;
    TreeNode<T>* parent;
    T element;
};

template <typename T>
TreeNode<T>::TreeNode(){
    children = new List<TreeNode<T>*>();
}

template <typename T>
T TreeNode<T>::getElement(){
    return element;
}

template <typename T>
void TreeNode<T>::setElement(T t){
    element = t;
}

template <typename T>
TreeNode<T>* TreeNode<T>::getParent(){
    return parent;
}

template <typename T>
TreeNode<T>* TreeNode<T>::getChild(int n){
    ListElement<TreeNode<T>*>* temp = children->getFirst();
    int i = 0;
    while( i<n && temp != NULL){
        temp = temp->getNext();
        ++i;
    }
    if(temp == NULL){
        throw "OutOfBoundsException";
    }else{
        return temp->getElement();
    }
}

template <typename T>
void TreeNode<T>::setParent(TreeNode<T>* p){
    parent = p;
}

template <typename T>
void TreeNode<T>::addChild(TreeNode<T>* c){
    children->addFront(c);
}

template <typename T>
void TreeNode<T>::removeChild(TreeNode<T>* c){
    ListElement<TreeNode<T>*>* temp = children->getFirst();
    List<TreeNode<T>*>* newChildren = new List<TreeNode<T>*>();
    while(temp!=NULL){
        if(temp->getElement() != c){
            newChildren->addFront(temp->getElement());
        }
        temp = temp->getNext();
    }

    children = newChildren;
}

template <typename T>
int TreeNode<T>::getSize(){
    return children->getSize();
}

template <typename T>
bool TreeNode<T>::isRoot(){
    return parent == NULL;
}

template <typename T>
bool TreeNode<T>::isExternal(){
    return children->isEmpty();
}

//=========================================================

template <typename T>
class Tree{
public:
    TreeNode<T>* getRoot();
    T getRootElement();

    int getHeight(TreeNode<T>* node);

    TreeNode<T>* addNode(TreeNode<T>* parent, T t);
    T removeNode(TreeNode<T>* node);

    bool isEmpty();

    void preOrder(TreeNode<T>* node);
    void postOrder(TreeNode<T>* node);

private:
    TreeNode<T>* root;
};

template <typename T>
TreeNode<T>* Tree<T>::getRoot(){
    return root;
}

template <typename T>
T Tree<T>::getRootElement(){
    if(!isEmpty()){
        return root->getElement();
    }else{
        throw "TreeEmptyException";
    }
}

template <typename T>
int Tree<T>::getHeight(TreeNode<T>* node){
    if(node->isExternal()){
        return 0;
    }else{
        int h = 0;
        for(int i=0; i<node->getSize(); ++i){
            h = std::max(h, getHeight(node->getChild(i)));
        }
        return h+1;
    }

}

template <typename T>
TreeNode<T>* Tree<T>::addNode(TreeNode<T>* parent, T t){
    TreeNode<T>* child = new TreeNode<T>();
    child->setElement(t);
    if(isEmpty()){
        child->setParent(NULL);
        root = child;
    }else{
        child->setParent(parent);
        parent->addChild(child);
    }
    return child;
}

template <typename T>
T Tree<T>::removeNode(TreeNode<T>* node){
    T value = node->getElement();
    if(!node->isRoot()){
        node->getParent()->removeChild(node);
    }
    delete node;
    return value;
}

template <typename T>
bool Tree<T>::isEmpty(){
    return root == NULL;
}

template <typename T>
void Tree<T>::preOrder(TreeNode<T>* node){
    if(node != NULL){
        cout<<node->getElement()<<"\n";
        for(int i=0; i<node->getSize(); ++i){
            preOrder(node->getChild(i));
        }
    }
}

template <typename T>
void Tree<T>::postOrder(TreeNode<T>* node){
    if(node != NULL){
        for(int i=0; i<node->getSize(); ++i){
            preOrder(node->getChild(i));
        }
        cout<<node->getElement()<<"\n";
    }
}
