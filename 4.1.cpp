#include "PriorityQueue.cpp"
#include <iostream>
using namespace std;

int main(){
    PriorityQueue<int>* kolejka = new PriorityQueue<int>();

    kolejka->insert(1, 1);
    kolejka->insert(4, 4);
    kolejka->insert(2, 2);
    kolejka->insert(3, 3);
    kolejka->insert(4, 144);

    while(!kolejka->isEmpty()){
        cout<<kolejka->removeMin()<<"\n";
    }

    return 0;

}

