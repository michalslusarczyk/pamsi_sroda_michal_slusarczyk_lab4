#include "Sequence.cpp"
#include <iostream>
using namespace std;


template <typename T>
class PriorityQueue:public Sequence<T>{
public:
    void insert(int k, T t);
    T removeMin();
    T getMin();
};


template <typename T>
void PriorityQueue<T>::insert(int k, T t){
    SequenceElement<T>* v = new SequenceElement<T>();
    v->setElement(t);
    v->setKey(k);
    v->setNext(NULL);
    v->setPrev(NULL);
    if(!this->isEmpty()){
        bool set = false;
        SequenceElement<T>* temp = this->first;
        while(!set && temp != NULL){
            if(temp->getKey()<k){
                temp = temp->getNext();
            }else{
                set = true;
                this->addBefore(temp, v);
            }
        }
        if(!set && temp==NULL){
            set = true;
            this->addRear(v);
        }
    }else{
        this->addFront(v);
    }
}

template <typename T>
T PriorityQueue<T>::removeMin(){
    T temp = this->removeFront();
    return temp;
}

template <typename T>
T PriorityQueue<T>::getMin(){
    return this->getFront();
}



