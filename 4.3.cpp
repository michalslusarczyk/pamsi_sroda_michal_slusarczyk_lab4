#include <iostream>
#include "BinaryTree.cpp"

using namespace std;


int main(){


    BinaryTree<int>* drzewko = new BinaryTree<int>();


    drzewko->addNode(NULL, 32);

    BinaryTreeNode<int>* A = drzewko->addNode(drzewko->getRoot(), 55);
    BinaryTreeNode<int>* B = drzewko->addNode(drzewko->getRoot(), 66);

    drzewko->addNode(A, 77);
    drzewko->addNode(A, 88);


    drzewko->addNode(B, 99);
    drzewko->addNode(B, 111);

    //drzewko->removeNode(B);

    cout<<drzewko->getHeight(drzewko->getRoot())<<"\n";

    cout<<"PRE ORDER \n";
    drzewko->preOrder(drzewko->getRoot());

    cout<<"IN ORDER \n";
    drzewko->inOrder(drzewko->getRoot());

    cout<<"POST ORDER \n";
    drzewko->postOrder(drzewko->getRoot());

    return 0;
}
